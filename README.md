#Recherche et Développement Starter

Little starter for **ThreeJS**

##Setup

####Download the dependencies
    npm i

####Build the structure
    grunt deploy
    
####If you want to run Browserify and the watchers
    grunt
    
####Your local server
`http://localhost:3000`
