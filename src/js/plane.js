import THREE from 'three'
import Hammer from './vendors/hammer'

export default class Plane {
  constructor() {
    this.scene
    this.camera
    this.renderer
    this.container = document.getElementById( 'container' )
    this.innerWidth = this.container.clientWidth
    this.innerHeight = this.container.clientHeight

    this.offsetX = 0
    this.offsetY = 0

    this.loader = new THREE.TextureLoader()

    this.plane

    this.init()
  }
  
  init() {
    this.scene = new THREE.Scene()

    this.camera = new THREE.PerspectiveCamera( 50, this.innerWidth / this.innerHeight, 1, 10000 )
    this.camera.position.set( 0, 0, 770 )
    this.camera.lookAt( 0, 0, 0 )
    this.scene.add( this.camera )
    
    this.renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } )
    this.renderer.setSize( this.innerWidth, this.innerHeight )
    this.renderer.setClearColor( 0xffffff, 0 )
    this.renderer.clear()
    this.container.appendChild( this.renderer.domElement )

    this.canvas = document.createElement( 'canvas' )
    this.canvas.width = this.innerWidth
    this.canvas.height = this.innerHeight

    this.context = this.canvas.getContext( '2d' )

    this.texture = new THREE.Texture( this.canvas )

    this.pattern = new Image()
    this.pattern.onload = () => {
      const drawingImage = this.context.createPattern( this.pattern, 'repeat' )
      this.context.rect( 0, 0, this.canvas.width, this.canvas.height )
      this.context.fillStyle = drawingImage
      this.context.fill()

      this.texture.needsUpdate = true

      const hammertime = new Hammer( document.body )
      hammertime.on( 'pan', ( e ) => {
          this.texture.needsUpdate = true
          this.offsetX = e.deltaX
          this.offsetY = e.deltaY
      } )

      // this.container.appendChild( this.canvas )
      this.createScene()
    }
    this.pattern.src = '../assets/images/pattern/pattern.png'
    
    // this.resize()
    // window.addEventListener( 'resize', this.resize.bind( this ), false )
  }
  
  createScene() {
    let geometry = new THREE.PlaneGeometry( this.innerWidth, this.innerHeight )
    let material = new THREE.MeshPhongMaterial( {
      map: this.texture,
      // shininess: 30,
      // side: THREE.DoubleSide,
      bumpMap: this.loader.load( '../assets/images/models/bump.png' ),
      alphaMap: this.loader.load( '../assets/images/models/mask.png' ),
      transparent: true
    } )

    this.plane = new THREE.Mesh( geometry, material )
    this.scene.add( this.plane )

    // this.light = new THREE.PointLight( 0xffffff )
    // this.light.position.set( 0, 0, 1000 )
    // this.light.castShadow = true
    // this.light.intensity = 0.7
    // this.light.decay = 0
    // this.light.distance = 0
    // this.light.lookAt( this.plane )
    // this.scene.add( this.light )

    this.animate()
  }
  
  animate() {
    this.render()
  }
  
  render() {
    window.requestAnimationFrame( this.animate.bind( this ) )
    
    this.move( this.offsetX, this.offsetY )

    this.renderer.render( this.scene, this.camera )
  }

  move( offsetX, offsetY ) {
    this.context.clearRect( 0, 0, this.canvas.width, this.canvas.height )
    this.context.save()
    this.context.translate( offsetX, offsetY )
    this.context.fillRect( -offsetX, -offsetY, this.innerWidth, this.innerHeight )
    this.context.restore()

    this.texture.needsUpdate = true
    this.plane.material.needsUpdate = true
    this.plane.material.map.needsUpdate = true

    this.plane.material.map = this.texture
  }
  
  resize() {
    this.innerWidth = window.innerWidth
    this.innerHeight = window.innerHeight

    this.camera.aspect = this.innerWidth / this.innerHeight
    this.camera.updateProjectionMatrix()

    this.renderer.setSize( this.innerWidth, this.innerHeight )
  }
}