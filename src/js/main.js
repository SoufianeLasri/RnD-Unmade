import domready from 'domready'
import Plane from './plane'

domready( () => {
    const plane = new Plane()
} )